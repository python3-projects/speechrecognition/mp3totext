#!/usr/bin/python3
import logging
from colorlog import ColoredFormatter


LOGFORMAT = "  %(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s"
formatter = ColoredFormatter(LOGFORMAT)
LOG_LEVEL = logging.DEBUG
logging.root.setLevel(LOG_LEVEL)
stream = logging.StreamHandler()
stream.setLevel(LOG_LEVEL)
stream.setFormatter(formatter)
log = logging.getLogger('pythonConfig')
log.setLevel(LOG_LEVEL)
log.addHandler(stream)

if __name__ == "__main__":
    log.debug("This is a debug message from logger")
    log.info("This is an info message from logger")
    log.warning("This is a warning message from logger")
    log.error("This is an error message from logger")
    log.critical("This is a critical message from logger")
    