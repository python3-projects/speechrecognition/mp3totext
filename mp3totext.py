#!/usr/bin/python3
from logger import log
import argparse

import os
import sys
import speech_recognition as sr
from pydub import AudioSegment


RELEASE = "v1.0.1"
AUTHOR = "Kamil Dębski"
CONTACT = "kamil_debski@pm.me"
CREATE_DATE = "01-02-2023"
MODIFY_DATE = "01-02-2023"

# Create the directories if they do not exist
mp3_dir = "mp3/"
wav_dir = "wav/"
text_dir = "text/"


def main():
    log.info(f"Executing: {sys.argv[0]}")
    log.info(f"Release: {RELEASE}")
    log.info(f"Author: {AUTHOR}, contact: {CONTACT}")
    log.info(f"Create date: {CREATE_DATE}, last modify date: {MODIFY_DATE}")
    
    parser = argparse.ArgumentParser(description='Convert mp3 file to text. Prerequisite is a "mp3" folder with .mp3 file/files. Polish language edition.')
    parser.add_argument("--debug", help="Print out the text which is saved in the text file.", action="store_true")
    args = parser.parse_args()

    if not os.path.exists(wav_dir):
        os.makedirs(wav_dir)
    if not os.path.exists(text_dir):
        os.makedirs(text_dir)

    # Check if there are any mp3 files in the directory
    if not os.listdir(mp3_dir):
        raise FileNotFoundError("No .mp3 files found in the directory 'mp3/'.")

    # Iterate over each .mp3 file in the directory
    for filename in os.listdir(mp3_dir):
        if filename.endswith(".mp3"):
            # Get the name of the file without the extension
            name, _ = os.path.splitext(filename)
            
            # Convert the .mp3 file to .wav
            wav_filename = name + ".wav"
            # Load the MP3 file
            mp3_file = AudioSegment.from_file(mp3_dir + filename, format="mp3")

            # Save the audio to a WAV file
            mp3_file.export(wav_dir + wav_filename, format="wav")
            
            # Create a SpeechRecognition object
            r = sr.Recognizer()
            
            # Recognize speech from the .wav file
            with sr.AudioFile(wav_dir + wav_filename) as source:
                audio = r.record(source)
            
            # Try to recognize the speech in the audio file
            try:
                text = r.recognize_google(audio, language="pl-PL")
                # Save the speech-to-text output to a text file
                text_filename = name + ".txt"
                if args.debug:
                    log.debug(text)
                with open(text_dir + text_filename, "w") as text_file:
                    text_file.write(text)
                    os.remove(wav_dir + wav_filename)
            except sr.UnknownValueError:
                log.error("Could not recognize speech")
            except sr.RequestError as e:
                log.error("Could not request results from Google Speech Recognition service: {0}".format(e))


if __name__ == "__main__":
    main()    
